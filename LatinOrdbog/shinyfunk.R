getUrl <- function(ord){
  url <- paste0("http://latinskordbog.dk/ordbog?query=",ord)
  return(url)
  
}

createUrlShortLewis <- function(ord){
  baseurl <- "http://perseus.uchicago.edu/cgi-bin/philologic/navigate.pl?lewisandshort."
  firstletter <- substr(ord,1,1)
  offset <- if(firstletter <= "i"){1} else {2}
  no <- which(firstletter == letters) - offset
  if(no < 0){
    print(no)
    print(ord)
    stop()
  }
  url <- paste0(baseurl,no)
}

getLinkToArticleShortLewis <- function(ord){
  url <- createUrlShortLewis(ord)
  html <- xml2::read_html(url)
  # text <- rvest::html_text(html)
  # nodes <- rvest::html_nodes(html,".navlevel1")
  # nodesWithWords <- rvest::html_nodes(html,".navlevel1")
  nodes <- rvest::html_nodes(html,"a")
  wordNO <- which(rvest::html_text(nodes) == ord)
  links <- rvest::html_attr(nodes,"href")
  url <- paste0("http://perseus.uchicago.edu/",links[wordNO])
  return(url)
}

getWordFromShortLewis <- function(ord){
  url <- getLinkToArticleShortLewis(ord)
  html <- xml2::read_html(url)
  result <- rvest::html_text(rvest::html_nodes(html,".sense"))
  result <- paste(result,collapse="\n")
  return(result)
}

afkortEksempler <- function(opslag,n){
  # Funktionen afkorter antal eksempler for et opslag
  # Eksempler er adskilt af semikolon
  
  # Der er et hierarki, der hedder I., 1), a), alpha)
  # Tjek, om karaktererne findes nedad i systemet
  
  # Check, om der er følgende streng I.
  test <- grepl("I.",opslag)
  test <- FALSE
  if(test){
    niv1 <- "(\\. I+\\.)"
    posOfBetydninger <- gregexpr(niv1,opslag)[[1]] + 2
    # Tjek, om der er underopdelinger
    for(p in 1:length(posOfBetydninger)){
      delbetydning <- substr(opslag,
                             posOfBetydninger[p],
                             posOfBetydninger[p+1] - 2)
      if(grepl("1)",delbetydning)){
        pos2 <- gregexpr("\\d)",delbetydning)[[1]]
        for(p2 in 1:length(pos2)){
          underdelbetydning <- substr(
            delbetydning,
            pos2[p2],
            pos2[p2+1] - 2
          )
          if(grepl("a)",underdelbetydning)){
            pos3 <- gregexpr("\\s[a-z])",underdelbetydning)[[1]]
            for(p3 in 1:length(pos3)){
              underdelbetydning2 <- substr(
                underdelbetydning,
                pos3[p3],
                pos3[p3+1] - 2
              )
              if(grepl("α",underdelbetydning2)){
                pos4 <- gregexpr("[α-ω])", underdelbetydning2)[[1]]
                underdelbetydning2 <- behandlBetydninger(pos4,
                                   underdelbetydning2,
                                   n)
              }
            }
          }
        }
      }
    }
    
    # browser()
  } else {
    niv1 <- "\\d)"
    posOfBetydninger <- gregexpr(niv1,opslag)[[1]]
    nyeBetydninger <- behandlBetydninger(posOfBetydninger,opslag,n)
    intro <- substr(opslag,1,posOfBetydninger[1] - 2)
    opslagForkortet <- paste(intro,nyeBetydninger)
  }

  return(opslagForkortet)
}

behandlBetydninger <- function(posOfBetydninger,opslag,n){
  nyeBetydninger <- ""
  for(p in 1:length(posOfBetydninger)){
    slut <- if(p == length(posOfBetydninger)){
      nchar(opslag)
    } else {
      posOfBetydninger[p+1] - 2
    }
    betydning <- substr(opslag,
                        posOfBetydninger[p],
                        slut
    )
    # Find eksempler
    betydning <- extractExamples(betydning,n)
    
    # Læg betydninger sammen igen
    nyeBetydninger <- paste(nyeBetydninger,betydning)
  }
  return(nyeBetydninger)
  
}


extractExamples <- function(betydning,n){
  # Find eksempler
  posOfExamples <- gregexpr(";",betydning)[[1]]
  # if(p == 3){browser()}
  if(posOfExamples[1] != -1){
    if(length(posOfExamples) + 1 > as.numeric(n)){
      # Afkort eksempler
      betydning <- substr(betydning,1,posOfExamples[as.numeric(n)])
    }
  }
  return(betydning)
}

getNamesOfDictionaires <- function(){
  r <- c("Latinsk-dansk ordbog","Lewis & Short")
  return(r)
}

hentOrdIBegge <- function(ord,noex){
  latinskOrdbog <- hentOrdFraLatinskOrdbog(ord,noex)
  sl <- getWordFromShortLewis(ord)
  return(c(latinskOrdbog,sl))
}

createDataFrameObject <- function(){
  # str <- "data.frame(Ord = newWord, 
  #         Kilde = dictionary, 
  #         Opslag = hentOrd(newWord, noex, dictionary))"
  str <- "data.frame(Ord = newWord, 
          'Latinsk Ordbog' = hentOrd(newWord, noex, getNamesOfDictionaires()[1]), 
          'Short & Lewis' = hentOrd(newWord, noex, getNamesOfDictionaires()[2]),
          check.names = FALSE)"
  return(str)
}

hentOrd <- function(ord,noex,dictionary){
  if(dictionary == getNamesOfDictionaires()[1]){
    r <- hentOrdFraLatinskOrdbog(ord,noex)
  }
  if(dictionary == getNamesOfDictionaires()[2]){
    r <- getWordFromShortLewis(ord)
  }  
  return(r)
}

hentOrdFraLatinskOrdbog <- function(ord,noex){
  url <- getUrl(ord)
  cc <- try(
    page <- xml2::read_html(url)
    ,silent = TRUE
  )
  if(is(cc,"try-error")){
    closeAllConnections()
    return("Intet resultat")
    # browser()
  }
  opslag <- rvest::html_text(rvest::html_nodes(page,".artikel")) 
  nchar <- gregexpr("\\n",opslag)[[1]]
  start <- nchar[3] + 5
  slut <- nchar[4] - 1
  r <- substr(opslag,start,slut)

   # Afkort eksempler, hvis denne er sat
  if(noex != "Alle"){
    r <- afkortEksempler(r,noex)
  }
  return(r)
}

